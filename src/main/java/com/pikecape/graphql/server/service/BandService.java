package com.pikecape.graphql.server.service;

import com.pikecape.graphql.server.domain.Band;
import com.pikecape.graphql.server.repository.BandRepository;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@GraphQLApi
public class BandService {

    private final BandRepository bandRepository;

    public BandService(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }

    /**
     * Get all bands.
     *
     * @return List<Band>
     */
    @GraphQLQuery(name = "bands")
    public List<Band> getBands() { return bandRepository.findAll();
    }

    /**
     * GET band by id.
     *
     * @param id
     * @return Band
     */
    @GraphQLQuery(name = "band")
    public Optional<Band> getBandById(@GraphQLArgument(name = "id") UUID id) {
        return bandRepository.findById(id);
    }

    /**
     * Create band.
     *
     * @param band
     * @return Band
     */
    @GraphQLMutation(name = "createBand")
    public Band createBand(@GraphQLArgument(name = "band") Band band) {
        return bandRepository.save(band);
    }

    /**
     * Update band.
     *
     * @param band
     * @return
     */
    @GraphQLMutation(name = "updateBand")
    public Band updateBand(@GraphQLArgument(name = "band") Band band) {
        Optional<Band> storedBand = bandRepository.findById(band.getId());

        if (storedBand.isPresent()) {
            storedBand.get().setName(band.getName());
            storedBand.get().setGenre(band.getGenre());

            return band;
        }

        return null;
    }

    /**
     * Delete band.
     *
     * @param id
     */
    @GraphQLMutation(name = "deleteBand")
    public void deleteBand(@GraphQLArgument(name = "id") UUID id) {
        bandRepository.deleteById(id);
    }

//    @GraphQLQuery(name = "isGood") // Calculated property of Food
//    public boolean isGood(@GraphQLContext Food food) {
//        return !Arrays.asList("Avocado", "Spam").contains(food.getName());
//    }
}
