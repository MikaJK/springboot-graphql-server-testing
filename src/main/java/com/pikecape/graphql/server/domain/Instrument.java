package com.pikecape.graphql.server.domain;

/**
 * Instrument.
 *
 */
public enum Instrument {
    GUITAR,
    BASS,
    DRUMS,
    VOCALS,
    KEYBOARD
}
