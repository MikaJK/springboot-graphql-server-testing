package com.pikecape.graphql.server.domain;

/**
 * Genre.
 *
 */
public enum Genre {
    ROCK,
    HARD_ROCK,
    PUNK_ROCK,
    METAL,
    POP
}
