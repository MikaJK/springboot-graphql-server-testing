package com.pikecape.graphql.server.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Song.
 * 
 * @author Mika J. Korpela
 */
@Table(name = "songs")
@Entity
public class Song implements Serializable
{
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name="UUID", strategy="org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", nullable = false)
    private UUID id;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "genre", nullable = true)
    private String genre;

    @Column(name = "duration", nullable = true)
    private Integer duration;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "band_id", nullable = false)
    private Band band;

    /**
     * No arguments constructor.
     */
    public Song() {
    }

    /**
     * Custom constructor.
     *
     * @param name
     * @param genre
     * @param duration
     * @param band
     */
    public Song(String name, String genre, Integer duration, Band band) {
        this.name = name;
        this.genre = genre;
        this.duration = duration;
        this.band = band;
    }

    // getters and setters.
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }
}